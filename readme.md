# Ionic Angular Test
Ⓒ 2022
Ronaldo Surdi
ronaldosurdi@gmail.com

### Git
git clone https://ronaldo-surdi@bitbucket.org/ronaldo-surdi/ionictest.git

---

### Requirements
- Ionic 6
- Npm
- Node

--- 

### Install
$ npm install

### Build
$ npm build

### Test
$ npm test

### Start
$ npm start
