import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  public title!: string;
  public restUri: string = environment.uri;

  public movies: any[] = [];
  public yearValue: any = null;
  public winnerValue: any = null;
  public totalPages = 0;
  public itemsPerPage = 10;
  public currentPage = 0;
  public firstDisabled: boolean = true;
  public priorDisabled: boolean = true;
  public nextDisabled: boolean = true;
  public lastDisabled: boolean = true;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.title = 'List Movies';

    this.restExec();
  }

  restExec() {
    // List Movies
    setTimeout(() => {
      let winnerParse: string = '';
      if (this.winnerValue != null && this.winnerValue != 'all') {
        winnerParse = '&winner=' + this.winnerValue;
      }
      let yearParse: string = '';
      if (this.yearValue != null) {
        yearParse = '&year=' + this.yearValue;
      }
      this.http.get(this.restUri + '?page=' + this.currentPage + '&size=' + this.itemsPerPage + winnerParse + yearParse).subscribe((data: any) => {
        console.log(data);
        this.movies = data.content;
        this.totalPages = data.totalPages;
        this.firstDisabled = (this.currentPage == 0);
        this.priorDisabled = (this.currentPage == 0);
        this.nextDisabled = (this.currentPage == this.totalPages-1);
        this.lastDisabled = (this.currentPage == this.totalPages-1);
        if (this.nextDisabled) {

        }
        console.log(this.movies);
      }, (error: any) => {
        console.log(error);
      });
    }, 100);
  }

  firstPage() {
    this.currentPage = 0;
    this.restExec();
  }
  priorPage() {
    this.currentPage--;
    this.restExec();
  }
  nextPage() {
    this.currentPage++;
    this.restExec();
  }
  lastPage() {
    this.currentPage = this.totalPages - 1;
    this.restExec();
  }

}
