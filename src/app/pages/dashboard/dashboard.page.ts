import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export interface Data {
  movies: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  public title!: string;
  public restUri: string = environment.uri;

  public years: any[] = [];
  public studios: any[] = [];
  public wins: any = [];
  public winners: any = [];
  public searchItens: any = [];
  public searchValue: any = null;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.title = 'Dashboard';

    // List years whith multiple winners
    this.http.get(this.restUri + '?projection=years-with-multiple-winners').subscribe((data: any) => {
      console.log(data);
      this.years = data.years;
      console.log(this.years);
    }, (error: any) => {
      console.log(error);
    });

    // Top 3 studios with winners
    this.http.get(this.restUri + '?projection=studios-with-win-count').subscribe((data: any) => {
      console.log(data);
      if (data.studios.length > 3) {
        for (let i=0; i < 3; i++) {
          this.studios.push(data.studios[i]);
        }
      } else {
        this.studios = data.studios;
      }
      console.log(this.years);
    }, (error: any) => {
      console.log(error);
    });

    // Producers with longest and shortest interval between wins
    this.http.get(this.restUri + '?projection=max-min-win-interval-for-producers').subscribe((data: any) => {
      console.log(data);
      this.wins = data;
      console.log(this.wins);
    }, (error: any) => {
      console.log(error);
    });
  }

  searchExec() {
    // List movie winners by year
    if (this.searchValue == null) {
      this.searchItens = [];
    } else {
      this.http.get(this.restUri + '?winner=true&year=' + this.searchValue).subscribe((data: any) => {
        console.log(data);
        this.searchItens = data;
        console.log(this.searchItens);
      }, (error: any) => {
        console.log(error);
      });
    }
  }

}
